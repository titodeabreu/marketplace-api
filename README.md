# Marketplace-api - An Assessment for Altran/Natixis

# Run Locally


##### Pre-requisite
 - mvn installed
 - docker installedx

### Execute app build:
    mvn clean install

### Execute docker build:
    docker build -f DockerFile -t marketplace .
    
### Create container by image
    docker run --name marketplace-api -p 21001:21001 marketplace