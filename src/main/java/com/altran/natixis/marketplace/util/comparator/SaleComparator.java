package com.altran.natixis.marketplace.util.comparator;

import com.altran.natixis.marketplace.model.dto.SaleDTO;

import java.util.Comparator;


public class SaleComparator implements Comparator<SaleDTO> {

    @Override
    public int compare(SaleDTO o1, SaleDTO o2) {
        int nameComparator = o1.getItemDTO().getName().compareTo(o2.getItemDTO().getName());
        if (nameComparator != 0)
            return nameComparator;
        else
            return o1.getItemDTO().getValue().compareTo(o2.getItemDTO().getValue());
    }
}
