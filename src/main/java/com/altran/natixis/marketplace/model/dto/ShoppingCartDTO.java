package com.altran.natixis.marketplace.model.dto;

import com.altran.natixis.marketplace.model.entity.Item;
import com.altran.natixis.marketplace.model.entity.Sale;
import com.altran.natixis.marketplace.model.entity.ShoppingCart;
import com.altran.natixis.marketplace.util.comparator.SaleComparator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public class ShoppingCartDTO {

    private String id;
    private String userId;
    private boolean shoppingCartOpen;
    private List<SaleDTO> sales = new ArrayList<>(0);
    private BigDecimal total;


    public ShoppingCartDTO(String id, String userId, boolean shoppingCartOpen) {
        this.id = id;
        this.userId = userId;
        this.shoppingCartOpen = shoppingCartOpen;
    }

    public ShoppingCartDTO(ShoppingCart shoppingCart) {
        this.id = shoppingCart.getId();
        this.userId = shoppingCart.getUserId();
        this.shoppingCartOpen = shoppingCart.isShoppingCartOpen();
    }

    public void createSalesDTO(List<Sale> sales, List<Item> items) {
        generateSalesWithItems(items);
        countItemsForShoppingCart(sales);
        sortSales();
    }

    private void sortSales() {
        Collections.sort(this.sales, new SaleComparator());
    }

    private void countItemsForShoppingCart(List<Sale> sales) {
        Map<String, Integer> countSales = new HashMap<>(0);
        for (Sale sale : sales) {
            if (countSales.get(sale.getItemId()) == null) {
                countSales.put(sale.getItemId(), 1);
            } else {
                countSales.put(sale.getItemId(), countSales.get(sale.getItemId()) + 1);
            }
        }

        this.sales.stream().forEach(sale -> sale.setQuantity(countSales.get(sale.getItemDTO().getId())));
    }

    private void generateSalesWithItems(List<Item> items) {
        for (Item item : items) {
            this.sales.add(new SaleDTO(new ItemDTO(item), 0));
        }
    }

    public void totalizatorShoppingCart() {
        BigDecimal finalValue = BigDecimal.ZERO;
        for (SaleDTO sale : sales) {
            finalValue = finalValue.add(sale.getItemDTO().getValue().multiply(BigDecimal.valueOf(sale.getQuantity())));
        }
        this.total = finalValue;
    }
}
