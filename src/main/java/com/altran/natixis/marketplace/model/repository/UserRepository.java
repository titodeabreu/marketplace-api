package com.altran.natixis.marketplace.model.repository;

import com.altran.natixis.marketplace.model.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

}
