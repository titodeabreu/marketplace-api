package com.altran.natixis.marketplace.model.repository.impl;

import com.altran.natixis.marketplace.model.entity.User;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserRepositoryDAO {

    private MongoTemplate mongoTemplate;

    public UserRepositoryDAO(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public boolean isUserNameRegistered(final String userName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(userName));
        List<User> users = mongoTemplate.find(query, User.class);
        return !users.isEmpty();
    }
}
