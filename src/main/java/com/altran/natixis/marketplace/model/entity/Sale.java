package com.altran.natixis.marketplace.model.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Sale")
@Getter
@Setter
public class Sale {

    @Id
    private String id;
    private String shoppingCartId;
    private String itemId;

    public Sale(String shoppingCartId, String itemId) {
        this.shoppingCartId = shoppingCartId;
        this.itemId = itemId;
    }
}
