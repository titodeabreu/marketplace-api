package com.altran.natixis.marketplace.model.repository;

import com.altran.natixis.marketplace.model.entity.Item;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends MongoRepository<Item, String> {
}
