package com.altran.natixis.marketplace.model.dto;

import com.altran.natixis.marketplace.model.entity.Item;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class ItemDTO {

    private String id;
    private String name;
    private BigDecimal value;


    public ItemDTO(String id, String name, BigDecimal value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public ItemDTO(Item item) {
        this.id = item.getId();
        this.name = item.getName();
        this.value = item.getValue();
    }
}
