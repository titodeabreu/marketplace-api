package com.altran.natixis.marketplace.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("User")
@Getter
@Setter
public class User {

    @Id
    private String id;
    private String name;
    private String mail;

    public User(String name, String mail) {
        this.name = name;
        this.mail = mail;
    }

    public User(String id, String name, String mail) {
        this.id = id;
        this.name = name;
        this.mail = mail;
    }

    public User() {
    }

    public User(String id) {
        this.id = id;
    }

}
