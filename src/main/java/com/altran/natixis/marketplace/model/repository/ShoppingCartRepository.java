package com.altran.natixis.marketplace.model.repository;

import com.altran.natixis.marketplace.model.entity.ShoppingCart;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShoppingCartRepository extends MongoRepository<ShoppingCart, String> {
    ShoppingCart findByUserId(String userId);
}
