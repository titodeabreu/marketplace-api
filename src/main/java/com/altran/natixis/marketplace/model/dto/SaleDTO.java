package com.altran.natixis.marketplace.model.dto;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SaleDTO {

    private ItemDTO itemDTO;
    private Integer quantity;

    public SaleDTO(ItemDTO itemDTO, Integer quantity) {
        this.itemDTO = itemDTO;
        this.quantity = quantity;
    }
}
