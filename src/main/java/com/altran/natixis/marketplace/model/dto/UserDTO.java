package com.altran.natixis.marketplace.model.dto;

import com.altran.natixis.marketplace.model.entity.User;
import lombok.Getter;

@Getter
public class UserDTO {

    private String id;
    private String name;
    private String mail;

    public UserDTO(String id, String name, String mail) {
        this.id = id;
        this.name = name;
        this.mail = mail;
    }

    public UserDTO(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.mail = user.getMail();
    }
}
