package com.altran.natixis.marketplace.model.repository;

import com.altran.natixis.marketplace.model.entity.Sale;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleRepository extends MongoRepository<Sale, String> {
    void deleteByShoppingCartId(String shoppingCartId);

    List<Sale> findAllByShoppingCartId(String shoppingCartId);
}
