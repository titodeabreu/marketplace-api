package com.altran.natixis.marketplace.model.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document("ShoppingCart")
@Getter
@Setter
@NoArgsConstructor
public class ShoppingCart {

    @Id
    private String id;
    private String userId;
    private boolean shoppingCartOpen;

    public ShoppingCart(String userId, boolean shoppingCartOpen) {
        this.userId = userId;
        this.shoppingCartOpen = shoppingCartOpen;
    }

    public ShoppingCart(String id) {
        this.id = id;
    }
}
