package com.altran.natixis.marketplace.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document("Item")
@Getter
@Setter
@NoArgsConstructor
public class Item {

    @Id
    private String id;
    private String name;
    private BigDecimal value;

    public Item(String name, BigDecimal value) {
        this.name = name;
        this.value = value;
    }

    public Item(String id) {
        this.id = id;
    }

    public Item(String id, String name, BigDecimal value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public Item(Item item) {
    }
}
