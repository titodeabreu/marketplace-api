package com.altran.natixis.marketplace.resource;

import com.altran.natixis.marketplace.model.dto.UserDTO;
import com.altran.natixis.marketplace.model.entity.User;
import com.altran.natixis.marketplace.service.ShoppingCartService;
import com.altran.natixis.marketplace.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/users"})
public class UserController {

    private UserService userService;
    private ShoppingCartService shoppingCartService;

    UserController(UserService userService, ShoppingCartService shoppingCartService) {
        this.userService = userService;
        this.shoppingCartService = shoppingCartService;
    }

    @GetMapping(path = {"/{id}"})
    public ResponseEntity<UserDTO> findById(@PathVariable String id) {
        UserDTO user = userService.findUser(new User(id));
        return ResponseEntity.ok().body(user);
    }

    @GetMapping
    public ResponseEntity<List<UserDTO>> findAll(){
        List<UserDTO> users = userService.retrieveAllUsers();
        return ResponseEntity.ok().body(users);
    }

    @PostMapping
    public ResponseEntity<UserDTO> create(@RequestBody UserDTO user){
        UserDTO userDTO = userService.createUser(new User(user.getName(),user.getMail()));
        return ResponseEntity.ok().body(userDTO);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<UserDTO> update(@RequestBody UserDTO user){
        UserDTO userDTO = userService.updateUser(new User(user.getId(),user.getName(),user.getMail()));
        return ResponseEntity.ok().body(userDTO);
    }

    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        userService.deleteUser(id);
        shoppingCartService.deleteShoppingCartByUser(id);
        return ResponseEntity.noContent().build();
    }

}
