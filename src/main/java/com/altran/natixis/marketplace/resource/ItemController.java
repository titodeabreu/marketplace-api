package com.altran.natixis.marketplace.resource;

import com.altran.natixis.marketplace.model.dto.ItemDTO;
import com.altran.natixis.marketplace.model.entity.Item;
import com.altran.natixis.marketplace.service.ItemService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/items"})
public class ItemController {

    private ItemService itemService;

    ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping(path = {"/{id}"})
    public ResponseEntity<ItemDTO> findById(@PathVariable String id) {
        ItemDTO item = itemService.findItem(new Item(id));
        return ResponseEntity.ok().body(item);
    }

    @GetMapping
    public ResponseEntity<List<ItemDTO>> findAll(){
        List<ItemDTO> items = itemService.retrieveAllItems();
        return ResponseEntity.ok().body(items);
    }

    @PostMapping
    public ResponseEntity<ItemDTO> create(@RequestBody ItemDTO item){
        ItemDTO itemDTO = itemService.createItem(new Item(item.getName(), item.getValue()));
        return ResponseEntity.ok().body(itemDTO);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<ItemDTO> update(@RequestBody ItemDTO item){
        ItemDTO itemDTO = itemService.updateItem(new Item(item.getId(),item.getName(),item.getValue()));
        return ResponseEntity.ok().body(itemDTO);
    }

    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        itemService.deleteItem(id);
        return ResponseEntity.noContent().build();
    }

}
