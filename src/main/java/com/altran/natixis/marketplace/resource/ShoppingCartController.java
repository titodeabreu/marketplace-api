package com.altran.natixis.marketplace.resource;

import com.altran.natixis.marketplace.model.dto.ShoppingCartDTO;
import com.altran.natixis.marketplace.service.ShoppingCartService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping({"/sc"})
public class ShoppingCartController {

    private ShoppingCartService shoppingCartService;

    ShoppingCartController(ShoppingCartService shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }

    @PostMapping(path ={"/{id}"})
    public ResponseEntity<ShoppingCartDTO> create(@PathVariable("id") String id){
        ShoppingCartDTO shoppingCartDTO = shoppingCartService.createShoppingCart(id);
        return ResponseEntity.ok().body(shoppingCartDTO);
    }

    @PostMapping(path ={"/{sc_id}/{item_id}"})
    public ResponseEntity<ShoppingCartDTO> add(@PathVariable("sc_id") String shoppingCartId,
                                               @PathVariable("item_id") String itemId){
        ShoppingCartDTO shoppingCartDTO = shoppingCartService.addItemIntoShoppingCart(shoppingCartId, itemId);
        return ResponseEntity.ok().body(shoppingCartDTO);
    }

    @GetMapping(path ={"/{id}"})
    public ResponseEntity<ShoppingCartDTO> retrieve(@PathVariable("id") String id){
        final ShoppingCartDTO shoppingCartDTO = shoppingCartService.retrieveShoppingCart(id);
        return ResponseEntity.ok().body(shoppingCartDTO);
    }

    @DeleteMapping(path ={"/cart/{id}"})
    public ResponseEntity<Void> deleteCart(@PathVariable("id") String id) {
        shoppingCartService.deleteShoppingCart(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path ={"/item/{sc_id}/{item_id}"})
    public ResponseEntity<Void> deleteItem(@PathVariable("sc_id") String shoppingCartId,
                                           @PathVariable("item_id") String itemId) {
        shoppingCartService.deleteItem(shoppingCartId,itemId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path ={"/clean/{id}"})
    public ResponseEntity<Void> cleanShoppingCart(@PathVariable("id") String shoppingCartId) {
        shoppingCartService.cleanShoppingCart(shoppingCartId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<ShoppingCartDTO> update(@PathVariable("id") String shoppingCartId){
        ShoppingCartDTO shoppingCartDTO = shoppingCartService.closeShoppingCart(shoppingCartId);
        return ResponseEntity.ok().body(shoppingCartDTO);
    }

}
