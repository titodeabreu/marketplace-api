package com.altran.natixis.marketplace.service;

import com.altran.natixis.marketplace.model.entity.Sale;
import com.altran.natixis.marketplace.model.repository.SaleRepository;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SaleService {

    private SaleRepository saleRepository;

    public SaleService(SaleRepository saleRepository) {
        this.saleRepository = saleRepository;
    }

    @Transactional
    public void addSale(String shoppingCartId, String itemId) {
        saleRepository.save(new Sale(shoppingCartId, itemId));
    }

    @Transactional
    public void deleteAllByShoppingCart(String shoppingCartId) {
        saleRepository.deleteByShoppingCartId(shoppingCartId);
    }

    @Transactional(readOnly = true)
    public List<Sale> retrieveSalesByShoppingCart(String shoppingCartId) {
        return saleRepository.findAllByShoppingCartId(shoppingCartId);
    }

    @Transactional
    public void deleteItemFromSale(String shoppingCartId, String itemId) {
        Optional<Sale> sale = saleRepository.findOne(Example.of(new Sale(shoppingCartId, itemId)));

        if(!sale.isPresent())
            throw new IllegalArgumentException("Item not found on Shopping Cart!");

        saleRepository.delete(sale.get());
    }
}
