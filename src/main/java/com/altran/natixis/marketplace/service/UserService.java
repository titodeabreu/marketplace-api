package com.altran.natixis.marketplace.service;

import com.altran.natixis.marketplace.model.dto.UserDTO;
import com.altran.natixis.marketplace.model.entity.User;
import com.altran.natixis.marketplace.model.repository.UserRepository;
import com.altran.natixis.marketplace.model.repository.impl.UserRepositoryDAO;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private UserRepository userRepository;
    private UserRepositoryDAO userRepositoryDAO;

    public UserService(UserRepository userRepository, UserRepositoryDAO userRepositoryDAO) {
        this.userRepository = userRepository;
        this.userRepositoryDAO = userRepositoryDAO;
    }

    @Transactional
    public UserDTO createUser(User user) {
        if (user == null || user.getId() != null || user.getName().isEmpty() || user.getMail().isEmpty())
            throw new IllegalArgumentException("Invalid User");
        if (userRepositoryDAO.isUserNameRegistered(user.getName()))
            throw new IllegalArgumentException("Invalid User Name");

        return new UserDTO(userRepository.save(user));
    }

    @Transactional(readOnly = true)
    public UserDTO findUser(User user) {
        Optional<User> userOptional = userRepository.findOne(Example.of(user));
        if (!userOptional.isPresent())
            throw new IllegalArgumentException("User Not Registered!");

        return new UserDTO(userOptional.get());
    }

    @Transactional(readOnly = true)
    public List<UserDTO> retrieveAllUsers() {
        return userRepository.findAll().stream().map(UserDTO::new).collect(Collectors.toList());
    }

    @Transactional
    public void deleteUser(String id) {
        userRepository.deleteById(id);
    }

    @Transactional
    public UserDTO updateUser(User user) {
        if (user == null || user.getId() == null || user.getName().isEmpty() || user.getMail().isEmpty())
            throw new IllegalArgumentException("Invalid User");

        return new UserDTO(userRepository.save(user));
    }

    @Transactional(readOnly = true)
    public boolean isUserExists(String id) {
        return userRepository.findById(id).isPresent();
    }
}
