package com.altran.natixis.marketplace.service;

import com.altran.natixis.marketplace.model.dto.ItemDTO;
import com.altran.natixis.marketplace.model.entity.Item;
import com.altran.natixis.marketplace.model.repository.ItemRepository;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ItemService {

    private ItemRepository itemRepository;

    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Transactional
    public ItemDTO createItem(Item item) {
        if (item == null || item.getId() != null || item.getName().isEmpty() || item.getValue() == null)
            throw new IllegalArgumentException("Invalid Item");

        if (item.getValue().compareTo(BigDecimal.ZERO) <= 0)
            throw new IllegalArgumentException("Invalid Item With Invalid Value");

        return new ItemDTO(itemRepository.save(item));
    }

    @Transactional(readOnly = true)
    public ItemDTO findItem(Item item) {
        Optional<Item> itemOptional = itemRepository.findOne(Example.of(item));
        if (!itemOptional.isPresent())
            return null;

        return new ItemDTO(itemOptional.get());
    }

    @Transactional(readOnly = true)
    public List<ItemDTO> retrieveAllItems() {
        return itemRepository.findAll().stream().map(ItemDTO::new).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<Item> retrieveAllByItemsIds(List<String> ids) {
        List<Item> items = new ArrayList<>(0);
        itemRepository.findAllById(ids).forEach(items::add);
        return items;
    }

    @Transactional
    public void deleteItem(String id) {
        itemRepository.deleteById(id);
    }

    @Transactional
    public ItemDTO updateItem(Item item) {
        if (item == null || item.getId() == null || item.getName().isEmpty() || item.getValue() == null)
            throw new IllegalArgumentException("Invalid Item");

        return new ItemDTO(itemRepository.save(item));
    }
}
