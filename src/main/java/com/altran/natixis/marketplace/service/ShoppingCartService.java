package com.altran.natixis.marketplace.service;

import com.altran.natixis.marketplace.model.dto.ShoppingCartDTO;
import com.altran.natixis.marketplace.model.entity.Item;
import com.altran.natixis.marketplace.model.entity.Sale;
import com.altran.natixis.marketplace.model.entity.ShoppingCart;
import com.altran.natixis.marketplace.model.repository.ShoppingCartRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ShoppingCartService {

    private ShoppingCartRepository shoppingCartRepository;
    private UserService userService;
    private SaleService saleService;
    private ItemService itemService;

    public ShoppingCartService(ShoppingCartRepository shoppingCartRepository, UserService userService,
                               SaleService saleService, ItemService itemService) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.userService = userService;
        this.saleService = saleService;
        this.itemService = itemService;
    }

    @Transactional
    public ShoppingCartDTO createShoppingCart(String userId) {
        if (!userService.isUserExists(userId))
            throw new IllegalArgumentException("User not Registered!");

        ShoppingCart createdShoppingCart = this.shoppingCartRepository.findByUserId(userId);
        if (createdShoppingCart != null)
            return retrieveShoppingCart(createdShoppingCart.getId());

        ShoppingCart persistedShoppingCart = shoppingCartRepository.save(new ShoppingCart(userId, true));
        return new ShoppingCartDTO(persistedShoppingCart.getId(), persistedShoppingCart.getUserId(), persistedShoppingCart.isShoppingCartOpen());
    }

    @Transactional(readOnly = true)
    public ShoppingCartDTO retrieveShoppingCart(ShoppingCart shoppingCart) {
        Optional<ShoppingCart> shoppingCartOptional = shoppingCartRepository.findById(shoppingCart.getId());
        if (!shoppingCartOptional.isPresent())
            throw new IllegalArgumentException("Shopping Cart Not Found!");

        ShoppingCartDTO shoppingCartDTO = new ShoppingCartDTO(shoppingCartOptional.get());
        createAndOrderSales(shoppingCartDTO);
        return shoppingCartDTO;
    }

    @Transactional(readOnly = true)
    public ShoppingCartDTO retrieveShoppingCart(String shoppingCartId) {
        return retrieveShoppingCart(new ShoppingCart(shoppingCartId));
    }

    @Transactional
    public ShoppingCartDTO addItemIntoShoppingCart(String shoppingCartId, String itemId) {
        if (shoppingCartId == null || shoppingCartId.isEmpty() || itemId == null || itemId.isEmpty())
            throw new IllegalArgumentException("Invalid Action!");

        this.saleService.addSale(shoppingCartId, itemId);
        return retrieveShoppingCart(shoppingCartId);
    }

    @Transactional
    public ShoppingCartDTO closeShoppingCart(String id) {
        Optional<ShoppingCart> shoppingCartOptional = shoppingCartRepository.findById(id);

        if (!shoppingCartOptional.isPresent())
            throw new IllegalArgumentException("Invalid Action!");

        ShoppingCart shoppingCart = shoppingCartOptional.get();
        shoppingCart.setShoppingCartOpen(false);
        shoppingCartRepository.save(shoppingCart);
        return retrieveShoppingCart(shoppingCart);
    }

    @Transactional
    public void deleteShoppingCartByUser(String id) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserId(id);
        deleteShoppingCart(shoppingCart.getId());
    }

    @Transactional
    public void deleteShoppingCart(String shoppingCartId) {
        saleService.deleteAllByShoppingCart(shoppingCartId);
        shoppingCartRepository.deleteById(shoppingCartId);
    }

    @Transactional
    public void deleteItem(String shoppingCartId, String itemId) {
        saleService.deleteItemFromSale(shoppingCartId,itemId);
    }

    @Transactional
    public void cleanShoppingCart(String shoppingCartId) {
        saleService.deleteAllByShoppingCart(shoppingCartId);
    }

    private void createAndOrderSales(ShoppingCartDTO shoppingCart) {
        List<Sale> sales = saleService.retrieveSalesByShoppingCart(shoppingCart.getId());
        List<Item> items = itemService.retrieveAllByItemsIds(sales.stream().map(Sale::getItemId).collect(Collectors.toList()));
        shoppingCart.createSalesDTO(sales, items);
        shoppingCart.totalizatorShoppingCart();
    }


}
