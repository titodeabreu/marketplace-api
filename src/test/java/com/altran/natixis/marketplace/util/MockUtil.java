package com.altran.natixis.marketplace.util;

import com.altran.natixis.marketplace.model.entity.Item;
import com.altran.natixis.marketplace.model.entity.ShoppingCart;
import com.altran.natixis.marketplace.model.entity.User;

import java.math.BigDecimal;

public class MockUtil {

    public static User createMockUser(String name, String mail) {
        return new User(name, mail);
    }

    public static ShoppingCart createMockShoppingCart(String userId, boolean shoppingCartOpen) {
        return new ShoppingCart(userId, shoppingCartOpen);
    }

    public static Item createMockItem(String name, BigDecimal value) {
        return new Item(name, value);
    }

    private MockUtil() {
    }
}
