package com.altran.natixis.marketplace.service;

import com.altran.natixis.marketplace.model.dto.UserDTO;
import com.altran.natixis.marketplace.model.entity.User;
import com.altran.natixis.marketplace.util.MockUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    private static final String MAIL_MOCK = "UserMock@gmail.com";

    @Autowired
    private UserService userService;

    @Test
    public void shouldCreateUser() {
        //Given
        User user = MockUtil.createMockUser("UserMock", MAIL_MOCK);
        //When
        userService.createUser(user);
        //Then
        String name = userService.findUser(user).getName();
        assertEquals("UserMock", name);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateUserWithEmptyName() {
        //Given
        User user = MockUtil.createMockUser("", MAIL_MOCK);
        //When
        userService.createUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateUserWithEmptyMail() {
        //Given
        User user = MockUtil.createMockUser("UserMock1", "");
        //When
        userService.createUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateUserWithNameRegistered() {
        //Given
        User user = MockUtil.createMockUser("UserMock2", "user@gmail.com");
        User userSecond = MockUtil.createMockUser("UserMock2", "user@gmail.com");
        userService.createUser(user);
        //When
        userService.createUser(userSecond);
    }

    @Test
    public void shouldFindAllUserRegistered() {
        //When
        User createdUser = MockUtil.createMockUser("AtLeastOneUserMock", MAIL_MOCK);
        userService.createUser(createdUser);
        List<UserDTO> users = userService.retrieveAllUsers();
        //Then
        assertNotNull(users);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldDeleteUserById() {
        //Given
        User user = MockUtil.createMockUser("OtherUserMock", MAIL_MOCK);
        userService.createUser(user);
        UserDTO persitedUser = userService.findUser(user);
        //When
        userService.deleteUser(persitedUser.getId());
        //Then
        UserDTO deleteUser = userService.findUser(user);
    }

    @Test
    public void shouldUpdateUserRegistered() {
        //Given
        User createdUser = MockUtil.createMockUser("CreatedUserMock", MAIL_MOCK);
        userService.createUser(createdUser);
        UserDTO persitedUser = userService.findUser(createdUser);
        assertEquals("CreatedUserMock", persitedUser.getName());
        User updatedUser = MockUtil.createMockUser("UpdatedUserMock", MAIL_MOCK);
        updatedUser.setId(persitedUser.getId());
        //When
        userService.updateUser(updatedUser);
        //Then
        UserDTO retrievedUser = userService.findUser(updatedUser);
        assertEquals("UpdatedUserMock", retrievedUser.getName());
        assertEquals(createdUser.getId(), retrievedUser.getId());
    }

    @Test
    public void shouldVerifyIfUserExists() {
        //Given
        User user = MockUtil.createMockUser("UserExists", MAIL_MOCK);
        UserDTO userDTO = userService.createUser(user);
        //When
        boolean userExists = userService.isUserExists(userDTO.getId());
        //Then
        assertTrue(userExists);
    }

    @Test
    public void shouldVerifyIfUserNotExists() {
        //When
        boolean userExists = userService.isUserExists("FAKE_ID");
        //Then
        assertFalse(userExists);
    }

}
