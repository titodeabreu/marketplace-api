package com.altran.natixis.marketplace.service;

import com.altran.natixis.marketplace.model.dto.ItemDTO;
import com.altran.natixis.marketplace.model.dto.ShoppingCartDTO;
import com.altran.natixis.marketplace.model.dto.UserDTO;
import com.altran.natixis.marketplace.model.entity.Item;
import com.altran.natixis.marketplace.model.entity.ShoppingCart;
import com.altran.natixis.marketplace.model.entity.User;
import com.altran.natixis.marketplace.util.MockUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShoppingCartServiceTest {

    private static final String MAIL_MOCK = "UserMock@gmail.com";

    @Autowired
    private UserService userService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private ItemService itemService;

    @Test
    public void shouldCreateShoppingCart() {
        //Given
        User user = MockUtil.createMockUser("ShoppingCartUser", MAIL_MOCK);
        UserDTO createdUser = userService.createUser(user);
        ShoppingCart mockShoppingCart = MockUtil.createMockShoppingCart(createdUser.getId(), true);
        //When
        ShoppingCartDTO shoppingCart = shoppingCartService.createShoppingCart(createdUser.getId());
        //Then
        ShoppingCartDTO createdShoppingCart = shoppingCartService.retrieveShoppingCart(shoppingCart.getId());
        assertNotNull(createdShoppingCart);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldCreateShoppingCartOnlyForRegisteredUsers() {
        //Given
        ShoppingCart mockShoppingCart = MockUtil.createMockShoppingCart("MOCK_ID", true);
        //When
        shoppingCartService.createShoppingCart("MOCK_ID");
        //Then
        shoppingCartService.retrieveShoppingCart(mockShoppingCart);
    }

    @Test
    public void shouldAddItemIntoShoppingCart() {
        //Given
        Item item = MockUtil.createMockItem("Item A", BigDecimal.valueOf(20));
        ItemDTO persitedItem = itemService.createItem(item);
        User user = MockUtil.createMockUser("UserSP1", MAIL_MOCK);
        UserDTO createdUser = userService.createUser(user);
        ShoppingCartDTO shoppingCart = shoppingCartService.createShoppingCart(user.getId());
        //When
        ShoppingCartDTO shoppingCartDTO = shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), item.getId());
        //Then
        assertNotNull(shoppingCartDTO);
        assertEquals(BigDecimal.valueOf(20), shoppingCartDTO.getTotal());
    }

    @Test
    public void shouldAddItemMultipleTimesIntoShoppingCart() {
        //Given
        Item item = MockUtil.createMockItem("Item A", BigDecimal.valueOf(20));
        ItemDTO persitedItem = itemService.createItem(item);
        User user = MockUtil.createMockUser("UserSP", MAIL_MOCK);
        UserDTO createdUser = userService.createUser(user);
        ShoppingCartDTO shoppingCart = shoppingCartService.createShoppingCart(user.getId());
        //When
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), item.getId());
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), item.getId());
        ShoppingCartDTO shoppingCartDTO = shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), item.getId());
        //Then
        assertNotNull(shoppingCartDTO);
        assertEquals(BigDecimal.valueOf(60), shoppingCartDTO.getTotal());
        assertEquals(3, shoppingCartDTO.getSales().get(0).getQuantity().intValue());
    }

    @Test
    public void shouldAddMultipleItemIntoShoppingCart() {
        //Given
        Item itema = MockUtil.createMockItem("Item A", BigDecimal.valueOf(2));
        ItemDTO persitedItema = itemService.createItem(itema);
        Item itemb = MockUtil.createMockItem("Item B", BigDecimal.valueOf(5));
        ItemDTO persitedItemb = itemService.createItem(itemb);
        Item itemc = MockUtil.createMockItem("Item C", BigDecimal.valueOf(10));
        ItemDTO persitedItemc = itemService.createItem(itemc);
        User user = MockUtil.createMockUser("UserSP2", MAIL_MOCK);
        UserDTO createdUser = userService.createUser(user);
        ShoppingCartDTO shoppingCart = shoppingCartService.createShoppingCart(user.getId());
        //When
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itema.getId());
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itemc.getId());
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itemc.getId());
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itemc.getId());
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itemb.getId());
        ShoppingCartDTO shoppingCartDTO = shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itemc.getId());
        //Then
        assertNotNull(shoppingCartDTO);
        assertEquals(BigDecimal.valueOf(47), shoppingCartDTO.getTotal());
    }

    @Test
    public void shouldAddMultipleItemAndOrderIntoShoppingCart() {
        //Given
        Item itema = MockUtil.createMockItem("Item A", BigDecimal.valueOf(2));
        ItemDTO persitedItema = itemService.createItem(itema);
        Item itemb = MockUtil.createMockItem("Item A", BigDecimal.valueOf(10));
        ItemDTO persitedItemb = itemService.createItem(itemb);
        Item itemc = MockUtil.createMockItem("Item A", BigDecimal.valueOf(5));
        ItemDTO persitedItemc = itemService.createItem(itemc);
        User user = MockUtil.createMockUser("UserSP3", MAIL_MOCK);
        UserDTO createdUser = userService.createUser(user);
        ShoppingCartDTO shoppingCart = shoppingCartService.createShoppingCart(user.getId());
        //When
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itema.getId());
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itemc.getId());
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itemc.getId());
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itemc.getId());
        shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itemb.getId());
        ShoppingCartDTO shoppingCartDTO = shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), itemc.getId());
        //Then
        assertNotNull(shoppingCartDTO);
        assertEquals(BigDecimal.valueOf(32), shoppingCartDTO.getTotal());
        assertEquals(persitedItemb.getId(), shoppingCartDTO.getSales().get(2).getItemDTO().getId());
    }

    @Test
    public void ShouldCloseShoppingCart() {
        //Given
        Item item = MockUtil.createMockItem("Item A", BigDecimal.valueOf(20));
        ItemDTO persitedItem = itemService.createItem(item);
        User user = MockUtil.createMockUser("UserSP5", MAIL_MOCK);
        UserDTO createdUser = userService.createUser(user);
        ShoppingCartDTO shoppingCart = shoppingCartService.createShoppingCart(user.getId());
        ShoppingCartDTO shoppingCartDTO = shoppingCartService.addItemIntoShoppingCart(shoppingCart.getId(), item.getId());
        //When
        shoppingCartService.closeShoppingCart(shoppingCartDTO.getId());
        //Then
        ShoppingCartDTO shoppingCartUpdated = shoppingCartService.retrieveShoppingCart(shoppingCart.getId());
        assertFalse(shoppingCartUpdated.isShoppingCartOpen());

    }

}
