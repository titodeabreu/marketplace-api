package com.altran.natixis.marketplace.service;

import com.altran.natixis.marketplace.model.dto.ItemDTO;
import com.altran.natixis.marketplace.model.entity.Item;
import com.altran.natixis.marketplace.util.MockUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemServiceTest {

    @Autowired
    private ItemService itemService;

    @Test
    public void shouldCreateItem() {
        //Given
        Item item = MockUtil.createMockItem("Item A", BigDecimal.valueOf(20));
        //When
        itemService.createItem(item);
        //Then
        String name = itemService.findItem(item).getName();
        assertEquals("Item A", name);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateItemWithEmptyName() {
        //Given
        Item item = MockUtil.createMockItem("", BigDecimal.valueOf(20));
        //When
        itemService.createItem(item);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateItemWithValue0() {
        //Given
        Item item = MockUtil.createMockItem("Item O", BigDecimal.valueOf(0));
        //When
        itemService.createItem(item);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateItemWithNegativeValue() {
        //Given
        Item item = MockUtil.createMockItem("Item O", BigDecimal.valueOf(-1));
        //When
        itemService.createItem(item);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateItemWithEmptyValue() {
        //Given
        Item item = MockUtil.createMockItem("Item B", null);
        //When
        itemService.createItem(item);
    }

    @Test
    public void shouldFindAllItemRegistered() {
        //When
        Item item = MockUtil.createMockItem("Item Z", BigDecimal.valueOf(20));
        itemService.createItem(item);
        List<ItemDTO> items = itemService.retrieveAllItems();
        //Then
        assertNotNull(items);
    }

    @Test
    public void shouldDeleteItemById() {
        //Given
        Item item = MockUtil.createMockItem("Item T", BigDecimal.valueOf(20));
        itemService.createItem(item);
        ItemDTO persitedItem = itemService.findItem(item);
        //When
        itemService.deleteItem(persitedItem.getId());
        //Then
        ItemDTO deleteItem = itemService.findItem(item);
        assertNull(deleteItem);
    }

    @Test
    public void shouldUpdateItemRegistered() {
        //Given
        Item createdItem = MockUtil.createMockItem("Item Created", BigDecimal.valueOf(20));
        itemService.createItem(createdItem);
        ItemDTO persitedItem = itemService.findItem(createdItem);
        assertEquals("Item Created", persitedItem.getName());
        Item updatedItem = MockUtil.createMockItem("Item Updated", BigDecimal.valueOf(20));
        updatedItem.setId(persitedItem.getId());
        //When
        itemService.updateItem(updatedItem);
        //Then
        ItemDTO retrievedItem = itemService.findItem(updatedItem);
        assertEquals("Item Updated", retrievedItem.getName());
        assertEquals(createdItem.getId(), retrievedItem.getId());
    }

}
